#include "FileSort.h"

int main(int argc, char *argv[])
{
    char* filePath = NULL;
    FILE* fPointer = NULL;
    void (*funcPointer)(int *, int) = NULL;
    bool time = false;

    char curChar;
    int numChar = 0;
    char* buff = NULL;

    int numData = 0;
    int* data = NULL;

    clock_t t,t2;

    //Checks if there are the corrct number of arguements
    if(argc > 4 || argc < 2)
    {
        //Notify the user how to use the program
        printf("Incorrect number of arguments.\nFileSort.exe [FLAGS] [File to Sort]\n-i: Insertion Sort\n-s: Selection Sort\n-q: Quick Sort\n-m: Merge Sort\n-t: Time Reporting(Only used if sorting flag is set");
        return 0;
    }
    else if(argc == 3)
    {
        //No timer flag so we know exactly where the flag is so grab the correct function
        funcPointer = functionPicker(argv[1][1]);
        //Set the file path from the arguments
        filePath = argv[2];
    }
    else if(argc == 4)
    {
        //Ternary to see if the first variable is the timer flag or the second and grab the correct function
        funcPointer = (argv[1][1] == 't') ? functionPicker(argv[2][1]) : functionPicker(argv[1][1]);
        //grab the filepath
        filePath = argv[3];
        //Set the boolean to tell to time it
        time = true;
    }
    else
    {
        //if only 2 arguements then we grab and go
        filePath = argv[1];
        funcPointer = insertionSort;
    }
 

    //allocate the buffer for reading in the numbers if there is a number thats longer than 1024 char then \_o_/
    buff = (char*) calloc(1024,sizeof(char));
    if(buff == NULL)
    {
        printf("Buffer memory couldn't be allocated.");
        return 0;
    }

    //allocate the buffer for reading in the numbers for a series
    data = (int*) calloc(1024,sizeof(int));
    if(data == NULL)
    {
        printf("Data Buffer memory couldn't be allocated.");
        return 0;
    }

    //Open the file and make sure it opened
    fPointer = fopen(filePath,"r");
    if(fPointer == NULL)
    {
        printf("Error Opening File");
        return 0;
    }

    //While we haven't hit the end of the file
    while((curChar = fgetc(fPointer)) != EOF)
    {
        //if there is a space since the data is space deliminated we take those characters and get the int from them
        if(curChar == ' ')
        {
            //take the string and convert it into a number and put it in our series
            data[numData] = atoi(buff);
            numData++;

            //Reset our character buffer to spaces
            memset(buff,' ',1024);
            numChar = 0;
        }
        //if it's a new line then the series is complete and time to parse the info
        else if(curChar == '\n')
        {
            //Makes sure there isn't a number in the buffer
            if(buff[numChar -1] != ' ')
            {
                data[numData] = atoi(buff);
                numData++;
            }

            //Calling the selected sorting algorithm through the function pointer wrapped in ternaries to grab the number of clock cycles the function takes.
            t = time ? clock(): 0;
            funcPointer(data,numData);
            t = time ? clock() - t: 0;

            //Print the info for the series
            dataPrint(data, numData, t, time);

            //Reset the arrays for the next series
            numData = 0;
            memset(buff,' ',1024);
            numChar = 0;
        }
        //if the other two don't happen then it should be a character to save
        else
        {
            buff[numChar] = curChar;
            numChar++;
        }
        
    }
    //Clean up our messiness
    free(buff);
    free(data);
    fclose(fPointer);
}

/********************************************************
 * INSERTION SORT
 * 
 * Takes the element and inserts it into the correct
 * position in the array that it's went over so far
 * https://en.wikipedia.org/wiki/Insertion_sort#:~:text=Insertion%20sort%20is%20a%20simple,%2C%20heapsort%2C%20or%20merge%20sort.
 *******************************************************/
void insertionSort(int* arr, int numElem)
{
    int key;
    int j;
    for (int i = 0; i < numElem; i++)
    {
        key = arr[i];
        j = i - 1;

        while (j >=0 && arr[j] > key)
        {
            arr[j+1] = arr[j];
            j = j -1;
        }
        arr[j+1]= key;
    }
}

/********************************************************
 * SELECTION SORT
 * 
 * Takes the element and goes through and swaps it's
 * place with where it goes into the array
 * https://en.wikipedia.org/wiki/Selection_sort
 *******************************************************/
void selectionSort(int* arr, int numElem)
{
    int indx;
    int tmp;

    for (int i = 0; i < numElem-1; i++)
    {
        indx = i;
        for (int j = i + 1; j < numElem; j++)
        {
            if(arr[j] < arr[indx])
            {
                indx = j;
            }
        }
        
        swap( &arr[indx], &arr[i]);
    }
}

/********************************************************
 * QUICK SORT
 * A divide and conguer algorithm. Picks an element then
 * reorder the values greater than it then recursively
 * partitions until everything is sorted
 *******************************************************/
void quickSortWrapper(int* arr, int numElem)
{
    quickSort(arr, 0, numElem - 1);
}

void quickSort(int* arr, int low, int high)
{
    if(low < high)
    {
        int pi = partition(arr, low, high);

        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

int partition (int* arr, int low, int high)
{
    int pivot = arr[high];
    int i = (low -1);

    for (int j = low; j <= high - 1; j++)
    {
        if(arr[j] < pivot)
        {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i+1], &arr[high]);
    return i + 1;
}


/********************************************************
 * MERGE SORT
 * A divide and conquer algorithm. It divides the array
 * into individual elements. It then repeatedly merges
 * them back into a new sorted array
 *******************************************************/
void mergeSortWrapper(int* arr, int numElem)
{
    mergeSort(arr, 0, numElem - 1);
}

void mergeSort(int* arr, int left, int right)
{
    if(left < right)
    {
        int middle = left + (right - left) / 2;

        mergeSort(arr, left, middle);
        mergeSort(arr, middle + 1, right);

        merge(arr, left, middle, right);
    }
}

void merge(int* arr, int left, int middle, int right)
{
    int i;
    int j;
    int k;
    int n1 = middle - left + 1;
    int n2 = right - middle;

    int* lArr = malloc(n1 * sizeof(int));
    int* rArr = malloc(n2 * sizeof(int));

    for (i = 0; i < n1; i++)
    {
        lArr[i] = arr[left + i];
    }
    for (j = 0; j < n2; j++)
    {
        rArr[j] = arr[middle + 1 + j];
    }

    i = 0;
    j = 0;
    k = left;

    while (i < n1 && j < n2)
    {
        if(lArr[i] <= rArr[j])
        {
            arr[k] = lArr[i];
            i++;
        }
        else
        {
            arr[k] = rArr[j];
            j++;
        }
        k++;
    }

    while(i < n1)
    {
        arr[k] = lArr[i];
        i++;
        k++;
    }

    while(j < n2)
    {
        arr[k] = rArr[j];
        j++;
        k++;
    }
    free(lArr);
    free(rArr);
}


/********************************************************
 * DATA PRINT
 * 
 * 
 *******************************************************/
void dataPrint(int arr[],int n, clock_t t, bool time)
{
    for (int i = 0; i < n; i++)
    {
        printf("%d ", arr[i]);
    }
    (time == true) ? printf(": %lf seconds\n", ((double) t)/CLOCKS_PER_SEC) : printf("\n");

}

/********************************************************
 * SWAP
 * 
 * Swaps two variables
 *******************************************************/
void swap(int* a, int* b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

/********************************************************
 * functionPicker
 * 
 * Takes the flag given by the user and selects the correct
 * sorting algorithm 
 *******************************************************/
void* functionPicker(char flag)
{
    switch (flag)
    {
    case 'i':
        printf("Insertion Sort : Time Taken\n------------------------------\n");
        return insertionSort;
        break;

    case 's':
        printf("Selection Sort : Time Taken\n------------------------------\n");
        return selectionSort;
        break;

    case 'q':
        printf("Quick Sort : Time Taken\n------------------------------\n");
        return quickSortWrapper;
        break;

    case 'm':
        printf("Merge Sort : Time Taken\n------------------------------\n");
        return mergeSortWrapper;
        break;
    
    default:
        printf("Incorrect number of arguments.\nFileSort.exe [FLAGS] [File to Sort]\n-i: Insertion Sort\n-s: Selection Sort\n-q: Quick Sort\n-m: Merge Sort");
        return 0;
    }
}

//*********************For Acclaim Badge*************************//
void acclaim()
{
    char str1[8] = "abc";
    char str2[8] = "def";
    char str3[8];

    strcpy(str3,str1);
    strcat(str3,str2);
    strlen(str3);
    printf(str3);
}